**Get Player**
----
    Fetch player's document.

* **URL**

    `/api/getPlayer/<user>`

* **Method:**

  `GET`
  
*  **URL Params**

   **Required:**
 
   `user=[string]`


* **Success Response:**
  

  * **Code:** 200 <br />
    **Content:** `{'_id': '25e07e76-cd77-11e8-a9e3-53120044312a', 'name': 'Blink', 'aspectsResearchedToday': 0, 'herbsResearchedToday': 0, 'knownHerbs': {'54': 'custom_name_placeholder'}, 'knownAspects': {}}`
 

* **Error Response:**


  * **Code:** 200 <br />
    **Content:** empty


* **Sample Call:**

`res = requests.get(HERBS_HOST + "/getPlayer/Blink")`