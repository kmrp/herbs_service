# Use an official Python runtime as a parent image
FROM python:3.6


# Add file to image for later use
ADD requirements.txt /requirements.txt

# Use pip to install dependencies
RUN pip install --trusted-host pypi.python.org -r /requirements.txt

# Set the working directory to /app
WORKDIR /app

# Copy the shared libraries to the container
ADD libraries /app/libraries

# Copy the importer directory contents into the container at /app
ADD herbs/* /app/src/
ADD templates/ /app/src/templates/


# Set environment variable pointing to container-local libraries
ENV PYTHONPATH /app
RUN ls src/templates/

# Run app.py when the container launches
CMD ["python", "src/app.py"]

