import pymongo
import json
import hashlib
import mymongo

from datetime import date
from random import randint
from flask import Flask, Response, request, render_template, redirect
from model import Herb
from constants import *

app = Flask(__name__)
client = pymongo.MongoClient(MONGO_HOST,
                              username=MONGO_USER,
                              password=MONGO_PASS)
db = client['herbs']

@app.route("/api/getPlayer/<user>")
def get_player(user):
    global client
    db = client['herbs']
    col = db['players']

    q = {'name': user}
    player = col.find_one(q)
    response = str(player)
    if not player:
        return Response(status=404)
    return Response(status=200, response=f"{response}")

@app.route("/api/addPlayer", methods=["POST"])
def add_player():

    name = request.form.get('name')
    uuid = request.form.get('uuid')
#    password = request.form.get('password')

    if not name:
        response = "Name is missing!"
    elif not uuid:
        response = "UUID is missing!"
#    elif not password:
#        response = "Password is missing!"
    else:
        response = mymongo.add_player(
                name = name,
                uuid = uuid
#                password = password
                )
    return Response(status=200, response=f"{response}")
    
@app.route("/api/addHerb", methods=["POST"])
def add_herb():
    herb = Herb(
            hid = request.form.get('id'),
            custom_name = request.form.get('custom_name'),
            name = request.form.get('name'),
            description = request.form.get('description'),
            item_id = request.form.get('item_id'),
            lore_name = request.form.get('lore_name'),
            lore_description = request.form.get('lore_description'),
            required_lvl = request.form.get('required_lvl'),
            difficulty = request.form.get('difficulty'),
            amount = request.form.get('amount'),
            aspects = request.form.get('aspects').split(','),
            biome = request.form.get('biome'),
            cost = request.form.get('cost'),
            secret = request.form.get('secret'),
    )
    response = mymongo.add_herb(herb)
    return Response(status=200, response=f"{response}")

@app.route("/api/addHerbKnowledge", methods=["POST"])
def add_herb_knowledge():
    name = request.form.get('name')
    herb_id = request.form.get('hid')

    if not name:
        response = "Name of player is missing!"
    elif not herb_id:
        response = "Herb ID is missing!"
    else:
        response = mymongo.add_herb_knowledge(name, herb_id)
    return Response(status=200, response=f"{response}")


@app.route("/api/getHerbsKnowledge/<user>")
def get_herbs_knowledge(user):
    response = mymongo.get_herbs_knowledge(user)
    return Response(status=200, response=f"{response}")


@app.route("/api/getAspectsKnowledge/<user>")
def get_aspects_knowledge(user):
    response = mymongo.get_aspects_knowledge(user)
    return Response(status=200, response=f"{response}")

@app.route("/api/getHerbInfo/<hid>")
def get_herb_info(hid):
    response = mymongo.get_herb_info(hid)
    return Response(status=200, response=f"{response}")


if __name__ == "__main__":
    app.run(host='0.0.0.0', port=80)
