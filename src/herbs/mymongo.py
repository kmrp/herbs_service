import pymongo
from constants import *
from model import Herb

client = pymongo.MongoClient(MONGO_HOST,
                             username=MONGO_USER,
                             password=MONGO_PASS)

def add_player(name: str, uuid: str):
    global client
    db = client["herbs"]
    col = db["players"]
    if col.find_one({'name': name}):
        return "Error! Name already in DB"
    if col.find_one({'_id': uuid}):
        return "Error! UUID already in DB"

    ins = {'_id': uuid, 'name': name, 'aspectsResearchedToday': 0, 'herbsResearchedToday': 0,
            'knownHerbs':{}, 'knownAspects':{}
            }
    col.insert_one(ins)
    return f"Player {name} was succesfully inserted"

def add_herb(herb: Herb):
    global client
    db = client["herbs"]
    col = db["herbs"]
    if col.find_one({'_id': herb.hid}):
        return "Error! Duplicate ID"
    ins = {
            '_id': herb.hid,
            'name': herb.name,
            'custom_name': herb.custom_name,
            'description': herb.description,
            'item_id': herb.item_id,
            'lore_name': herb.lore_name,
            'lore_description': herb.lore_description,
            'required_lvl': herb.required_lvl,
            'difficulty': herb.difficulty,
            'amount': herb.amount,
            'aspects': herb.aspects,
            'biome': herb.biome,
            'cost': herb.cost,
            'secret': herb.secret
            }

    col.insert_one(ins)
    return f"Herb {herb.name} successfully inserted!"


def add_herb_knowledge(name: str, herb_id: int):
    global client
    db = client["herbs"]
    col = db["herbs"]
    if not col.find_one({'_id': herb_id}):
        return "Error! No herb with this ID: \'" + herb_id + "\'"
    col = db["players"]
    if not col.find_one({'name': name}):
        return "Error! No player with this name: \'" + name + "\'"

    player = col.find_one({'name': name})
    herbs = player.get('knownHerbs')
    herbs[herb_id] = 'custom_name_placeholder'
    col.update_one(
            {'name': name},
            {"$set":{'knownHerbs': herbs}}
            )
    return f"Knowledge of {herb_id} was added to {name}"


def get_herbs_knowledge(user):
    global client
    db = client['herbs']
    col = db['players']

    q = {'name': user}
    player = col.find_one(q)

    if not player:
        return f"Error! Player {user} not found."

    return player.get('knownHerbs')

def get_aspects_knowledge(user):
    global client
    db = client['herbs']
    col = db['players']

    q = {'name': user}
    player = col.find_one(q)
    if not player:
        return f"Error! Player {user} not found."

    herbs_ids = player.get('knownHerbs')
    knownAspects = player.get('knownAspects')

    col = db['herbs']
    knowledge = []
    for hid in herbs_ids:
        q = {'_id': hid}
        doc = col.find_one(q)
        return knownAspects
        for i in range (0,4):
            if i+1 in knownAspects[hid]:
                knowledge.append((hid, i+1, doc.get('aspects')[i]))

    return knowledge

def get_herb_info(hid):
    global client
    db = client['herbs']
    col = db['herbs']

    q = {"_id": hid}
    herb = col.find_one(q)
    if not herb:
        return f"Error! Herb with ID {hid} not found!"
    return herb
