from pydantic import BaseModel 
from typing import Optional, List

class Herb(BaseModel):
    hid: str
    custom_name: Optional[str] = "custom_name_placeholder"
    name: str
    description: str
    item_id: str
    lore_name: str
    lore_description: str

    required_lvl: Optional[int]  # Минимальный навык поиска (заодно отвечает за отображение в списке)
    difficulty: int  # Сложность нахождения
    amount: int  # Модификатор количества (d)
    aspects: List[str]  # Аспекты (сложность от положения в списке)
    biome: str = None # Ареал обитания (биом, можно оставить пустым и записать координаты)
#    coordinates: List[float]  # Координаты (тут надо будет подумать над формой)
    cost: float = 1 # Стоимость экшенпоинтов (дефолт 1, отнимается больше при особой стоимости)
    secret: bool = False  # Секретность травы (булево, если true то не появляется в списке, даже если хватает навыка)

    def set_aspects(self, aspects_str):
        aspects = apsects_str.split(',')

