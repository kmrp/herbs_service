# your matrikel number as prefix for the software stack
MATRIKEL_NR=01467816

# setting proper paths
PROJECT_PATH=$(shell pwd)

# global config paramters for docker compose
COMPOSE_ARGS=--file docker-compose.yaml -p ${MATRIKEL_NR}

# build all images
images:
	docker build -t imse/${MATRIKEL_NR}_filler -f ${PROJECT_PATH}/src/filler.dockerfile ${PROJECT_PATH}/src
	docker build -t imse/${MATRIKEL_NR}_ms2 -f ${PROJECT_PATH}/src/MS2.dockerfile ${PROJECT_PATH}/src
	# docker build -t imse/${MATRIKEL_NR}_migrator -f migrator.dockerfile ${PROJECT_PATH}src
	# docker build -t imse/${MATRIKEL_NR}_MS3 -f MS3.dockerfile ${PROJECT_PATH}/src

# start DB creation
filldb:
	docker run \
	--rm \
	-it \
	--network ${MATRIKEL_NR}_imsenet \
	-v ${PROJECT_PATH}/src/filler:/app/src \
	-v ${PROJECT_PATH}/src/libraries:/app/libraries \
	-e "BOOKS=100" \
	-e "USER=10" \
	-e "AUTHORS=10" \
	-e "RENTALS=20" \
	imse/${MATRIKEL_NR}_filler

# start DB migration
migrate:
	docker run imse/${MATRIKEL_NR}_migrator

web:
	docker-compose ${COMPOSE_ARGS} up web
	# docker-compose ${COMPOSE_ARGS} up --detach web

# start all DBs and admin tools
db.up:
	docker-compose ${COMPOSE_ARGS} up --detach mongo mongo-express mariadb phpmyadmin

# stop all containers in stack
down:
	docker-compose ${COMPOSE_ARGS} down

# start MS2 web app
ms2:
	docker run \
	--name ${MATRIKEL_NR}_api \
	--rm \
	-it \
	--network ${MATRIKEL_NR}_imsenet \
	-v ${PROJECT_PATH}/src/MS2:/app/src \
	-v ${PROJECT_PATH}/src/libraries:/app/libraries \
	imse/${MATRIKEL_NR}_ms2


# start MS3 web app
ms3:
	docker run \
	--rm \
	-it \
	--network ${MATRIKEL_NR}_imsenet \
	-v ${PROJECT_PATH}/src/MS3:/app/src \
	-v ${PROJECT_PATH}/src/libraries:/app/libraries \
	imse/${MATRIKEL_NR}_ms3


# start portainer as Docker managment tool
portainer:
	docker run -d -p 9000:9000 --name portainer -v /var/run/docker.sock:/var/run/docker.sock -v portainer_data:/data portainer/portainer
